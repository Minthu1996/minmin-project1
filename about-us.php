<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>About us</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="fontawesome/css/all.min.css">
</head>
<body>

<?php include("nav.php");?>

<!--Banner-->
<div class="banner-about-us">
<div class="container-fluid">
<h2 class="d-flex justify-content-center" style="color:white;">About Us</h2> 
</div>
</div>

<!--Article-->
<div class="container">
<div class="row">
<div class="col-md-3 col-xs-6 px-2">
<div class=" service-box">
<img src="images/production.png" class="img-fluid" alt="">
<p>Video production</p>
</div>
</div>
<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/design.png" class="img-fluid" alt="">
<p>Graphic Design & Printing</p>
</div>
</div>

<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/production.png" class="img-fluid" alt="">
<p>VFX,3D and Visualization</p>
</div>
</div>
<div class="col-md-3 col-xs-6 px-2">
<div class="service-box">
<img src="images/event.png" class="img-fluid" alt="">
<p>Event Management</p>
</div>
</div>
</div>
</div>



<section class="content-about-us p-5">
<div class="container">

<div class="row">

<p>Myanmar Media Linkage is initially established as a partnership business by local media and IT professionals in 2012.It was reformed into a company limited in 2014 to
expand business and to be more competitive in the market with our innovative and creative services.We mainly expertise in two sectors,media production and IT soultions.
</p>


<p>We are a leading web development company with our in-house local designers and developers that specialize in providing IT and web solutions.From web design
web development,eCommerce platforms to any website and applications,you name it,we've done it.
</p>


<p>
We produce commercial, entertainment, corporate and industrial media. We handle full-scale concept development, copy writing, advertising materials, graphics, web
and mobile programming, script writing, video and film. We produce self-published and contracted media projects across industries and in any format. In addition to 
content development and content creation, we offer full range of event planning, publishing services including manufacturing and distributions.
</p>


<p>
We’ve worked with many government agencies, notable big brands, SMEs and entrepreneurs, such as Government of Ayeyarwaddy Region, Myanmar Posts and
Telecommunications (MPT), Myanmar Rice Federation (MRF) and Myanmar Agribusiness Public Corporation (MAPCO), etc. We are dedicated to using all that experience
to benefit businesses.

</p>


</div>
</div>
</section> 


<!--Footer-->
<footer class="footer-area">
<div class="container-fluid">
<div class="row text-center">
<div class="col-md-4 sm-12 p-4 bg-ingfo">
<i class="far fa-envelope"></i>
Email <br>
info@mml.com.mm
</div>

<div class="col-md-4 sm-12 p-4 bg-time">
<i class="far fa-clock"></i>
Working hours <br>
Mon-Fri 9AM - 6PM
</div>

<div class="col-md-4  sm-12 p-4 bg-contact">
<i class="fas fa-phone-alt"></i>
Telephone <br>
+959 9759 09701 <br>
+959 4411 95962
</div>
</div>
</div>


<section class="info">
<div class="container my-5">
<div class="row">
<div class="col-md-4">
<h6>HEAD OFFICE</h6>
<p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
Street Beside the concrete Express way Wardan Port Area
Lanmadaw Township Yangon
</p><br>


<h6>OFFICE ADDRESS</h6>
<p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
Street Beside the concrete Express way Wardan Port Area
Lanmadaw Township Yangon</p>

</div>

<div class="col-md-4 ">
<h6>USEFUL LINKS</h6>

<div class="row">
<div class="col-md-6">
<ul class="list-unstyled">
<li><a href="index.php">Home</a></li>
<li><a href="services.php">Services</a></li>
<li><a href="news.php">News</a></li>
<li><a href="career.php">Career</a></li>
<li><a href="about-us.php">About us</a></li>
</ul>

</div>
<div class="col-md-6">
<ul class="list-unstyled">
<li>Carrer</li>
<li>Reviews</li>
<li>Terms & Conditions</li>
<li>Help</li>
<li>Events</li>
</ul>
</div>
</div>


</div>


<div class="col-md-4">
<h6>Events</h6>


<div class="container my-3">
<div class="row">
<div class="col-md-6">
<img src="images/mml1.jpg" class="img-fluid"alt="">
</div>
<div class="col-md-6">
<img src="images/mml2.jpg" class="img-fluid"alt="">
</div>
</div>

<div class="row">
<div class="col-md-6">
<img src="images/mml3.jpg" class="img-fluid"alt="">
</div>
<div class="col-md-6">
<img src="images/mml4.jpg" class="img-fluid"alt="">
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</section>

<div class="copyright-wrap py-3">
<div class="container">
<div class="row">
<div class="col-12">
<div class="copyright-text text-center">

<p>© Copyright 2019 Myanmar Media Linkage. All rights reserved.</p>

</div>
</div>
</div>
</div>
</div>

</div>


</footer>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-min.js"></script>
